import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;

public class FileHandler {
	
	public static boolean checkForFile(String filename){
		File file = new File(filename + ".txt");
		if (file.exists())return true;
		else return false;
	}
	
	public static ArrayList<AddressBookEntry> getFileContents(){
		ArrayList<AddressBookEntry> addressBook = new ArrayList<AddressBookEntry>(); 
		
	    addressBook = parseFileContents(addressBook);
		return addressBook;
	}
	
	public static void writeToFile(ArrayList<AddressBookEntry> addressBook){
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(
		              new FileOutputStream("addressBook.txt"), "utf-8"))) {
			String data = "";
			for (AddressBookEntry addressBookEntry : addressBook) {
				data += addressBookEntry.toString();
			}
			writer.write(data);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Changes the contents of the file from a long string into 
	 * individual addressBookEntry objects with their relevant data.
	 * 
	 * @param ab
	 * @return ArrayList<AddressBookEntry>
	 */
	public static ArrayList<AddressBookEntry> parseFileContents(ArrayList<AddressBookEntry> ab){
		File file = new File("addressBook.txt");
		try(BufferedReader br = new BufferedReader(new FileReader(file))){
			String fileContents = br.readLine();
			String[] data = fileContents.split(",");
			
			for(int i = 0; i < data.length; i+=5){
				String name = data[i];
				String address = data[i+1];
				String phoneNum = data[i+2];
				String email = data[i+3];
				String zipCode = data[i+4];
				
				ab.add(new AddressBookEntry(name, address, phoneNum, email, zipCode));
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
				
		return ab;
	}
}
