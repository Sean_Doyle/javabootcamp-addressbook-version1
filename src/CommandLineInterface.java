import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

public class CommandLineInterface {
	private boolean isRunning;
	private boolean changesToBeSaved;
	private ArrayList<AddressBookEntry> addressBook;
	private BufferedReader br;
	
	public CommandLineInterface(){
		isRunning = true;
		changesToBeSaved = false;
		addressBook = new ArrayList<AddressBookEntry>();
		br = new BufferedReader(new InputStreamReader(System.in));
	}
	
	public void run(){
		if(FileHandler.checkForFile("AddressBook"))
			addressBook = FileHandler.getFileContents();
		
		while(isRunning){
			
			printMainMenu();
			
			try {
				executeSelection(Integer.parseInt(br.readLine()));
			} catch (NumberFormatException | IOException e) {
				e.printStackTrace();
			}
		}
		
		save();
		System.out.println("Application Closed.");
	}
	
	public void save(){
		if(changesToBeSaved){
			boolean validInput = true;
			do{
				try {
					System.out.print("Would you like to save your changes?(y/n):");
					String response = br.readLine().toLowerCase();
					
					if(response.charAt(0) == 'y'){ 
						FileHandler.writeToFile(addressBook);
						System.out.print("Changes have been saved! \n\n");
						changesToBeSaved = false;
					}
					else if(response.charAt(0) != 'n'){
						System.out.println("Invalid Input! \n\n");
						validInput = false;
					}
					else System.out.println("Changes were not saved! \n\n");
					
				} catch (IOException e) {
					e.printStackTrace();
				}
			}while(!validInput);
		}
	}
	
	public void executeSelection(int userInput){
		switch(userInput){
			case 1: printAddressBook();
				break;
			case 2: addContact();
				break;
			case 3: updateContact();
				break;
			case 4: deleteContact();
				break;
			case 5: save();
				break;
			case 0: isRunning = false;
				break;
			default:
				System.out.println("Invalid Input! Input must be 0 -> 5");
				break;		
		}			
	}
	
	public void deleteContact(){	
		printAddressBook();
		try {
			System.out.print("Who would you like to remove from the list?:");
			String userInput = br.readLine();
			int userIndex = getUserIndex(userInput);
			
			if(userIndex >= 0){
				addressBook.remove(userIndex);
				changesToBeSaved = true;
			}
			else System.out.println("Invalid Entry!");
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
		
	public void updateContact(){
		printAddressBook();
		try{
			System.out.println("Whose data would you like to change?(Enter full name):");
			String userInput = br.readLine();
			int userIndex = getUserIndex(userInput);
			
			AddressBookEntry abe;
			if(userIndex >= 0){
				abe = addressBook.get(userIndex);
				
				System.out.print(" Enter a new address:"); String address = br.readLine();
				System.out.print(" Enter a new phone number:"); String phoneNum = br.readLine();
				System.out.print(" Enter a new email address"); String email = br.readLine();
				System.out.print(" Enter a new zip code:"); String zipCode = br.readLine();
				
				abe.setAddress(address);
				abe.setPhoneNum(phoneNum);
				abe.setEmail(email);
				abe.setZipCode(zipCode);
				
				changesToBeSaved = true;
			}
			else System.out.println("Invalid Entry!");			
		}catch (IOException e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Finds the index in the addressBook of a specified user with a brute force search.
	 * @param user
	 * @return The index of the user or -1 if invalid user.
	 */
	public int getUserIndex(String user){
		for (int i = 0; i < addressBook.size(); i++) {
			if(addressBook.get(i).getName().equals(user)) return i;
		}
		return -1;
	}
	
	public void addContact(){
		System.out.println("\n &=== NEW ENTRY ===&");
		try{
			System.out.print(" Name?: "); String name = br.readLine();
			System.out.print(" Address?: "); String address = br.readLine();
			System.out.print(" Phone Number?: "); String phoneNum = br.readLine();
			System.out.print(" Email?: "); String email = br.readLine();
			System.out.print(" Zip Code?: "); String zipCode = br.readLine();
			
			addressBook.add(new AddressBookEntry(name,address,phoneNum,email,zipCode));
			changesToBeSaved = true;
			System.out.println("\n Contact Details Added!");
			
		} catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public void printAddressBook(){
		System.out.println("\n &=== CONTACTS ===&");
		for (AddressBookEntry addressBookEntry : addressBook) {
			System.out.println(addressBookEntry.formatedToString() + "\n");
		}
		
		System.out.print("Press 'n' to sort by name or \nPress 'z' to sort by zip code or \nPress Enter to continue: ");
		String input = ""; 
		
		try{ input = br.readLine().toLowerCase(); } 
		catch(IOException e){ e.printStackTrace(); }
		
		if(input.equals("n")) sortList(true); 		
		else if(input.equals("z")) sortList(false);
	}
	
	private void sortList(Boolean sortByName){
		ArrayList<AddressBookEntry> list = new ArrayList<AddressBookEntry>(addressBook);
		
		if(sortByName) Collections.sort(list, AddressBookEntry.abeNameSort);
		else Collections.sort(list, AddressBookEntry.abeZipSort);
		
		System.out.println("\n &=== SORTED CONTACTS ===&");
		for (AddressBookEntry addressBookEntry : list) {
			System.out.println(addressBookEntry.formatedToString() + "\n");
		}
	}
		
	public void printMainMenu(){
		String msg =
				" &=== MAIN MENU ===& \n"
				+ "Welcome to the simple address book app! \n"
				+ "The following options are available to you: \n"
				+ "1) View Current Contacts. \n"
				+ "2) Add a new Contact. \n"
				+ "3) Update a Contact. \n"
				+ "4) Remove a Contact. \n"
				+ "5) Save changes. \n"
				+ "0) Exit. \n \n"
				+ "Please make your choice(0-5):";
		
		System.out.print(msg);		
	}
		
	public static void main(String[] args){
		new CommandLineInterface().run();
	}
}
