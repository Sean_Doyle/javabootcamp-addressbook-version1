import java.util.Comparator;

public class AddressBookEntry {
	
	private String
		name,
		address,
		phoneNum,
		email,
		zipCode;
	
	public AddressBookEntry(String name, String address, String phoneNum, String email, String zipCode){
		this.name = name;
		this.address = address;
		this.phoneNum = phoneNum;
		this.email = email;
		this.zipCode = zipCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	
	public String toString(){
		return name + "," + address + "," + phoneNum + "," + email + "," + zipCode + ",";
	}
	
	public String formatedToString(){
		return " Name: " + name +
				"\n Address: " + address +
				"\n Phone: "+ phoneNum +
				"\n Email: " + email +
				"\n Zip Code: " + zipCode;
	}

	public static Comparator<AddressBookEntry> abeNameSort = new Comparator<AddressBookEntry>(){		
		public int compare(AddressBookEntry abe1, AddressBookEntry abe2){
			String abe1Name = abe1.getName().toLowerCase();
			String abe2Name = abe2.getName().toLowerCase();
			
			return abe1Name.compareTo(abe2Name);
		}
	};
	
	public static Comparator<AddressBookEntry> abeZipSort = new Comparator<AddressBookEntry>(){		
		public int compare(AddressBookEntry abe1, AddressBookEntry abe2){
			String abe1Zip = abe1.getZipCode().toLowerCase();
			String abe2Zip = abe2.getZipCode().toLowerCase();
			
			return abe1Zip.compareTo(abe2Zip);
		}
	};
}
 
